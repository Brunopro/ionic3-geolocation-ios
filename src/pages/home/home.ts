import { Component } from '@angular/core';
import { NavController, AlertController, Platform, NavParams, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  lat: any;
  long: any;
  gotPosition: boolean = false;

  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  hasRequestedLocation: boolean = false;

  constructor(
    public navCtrl: NavController,
    private geolocation: Geolocation,
    private platform: Platform,
    private diagnostic: Diagnostic,
    public loadingCtrl: LoadingController,
    private alertCtrl:AlertController,
    ) {

  }


  locationIos() {


    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });
    authorizationLoader.present();


    this.diagnostic.isLocationAuthorized().then((resp) => {
      authorizationLoader.dismiss().then(() => {
        this.gaveLocationPermission = resp;
        if (this.gaveLocationPermission) {
          let enablingLoader = this.loadingCtrl.create({
            content: "Checando status do GPS..."
          });
          enablingLoader.present();
          console.log('Gave location permission')
          this.diagnostic.isLocationEnabled().then((resp) => {
            console.log('Location is enabled : ' +resp)
            enablingLoader.dismiss()
            console.log('Enabling loader dismissed')
            this.doneChecking = true;
            this.hasEnabledLocation = resp;
            if (this.hasEnabledLocation) {
              this.getPosition();
            }
          });
        } else {
          this.gotPosition = false
          //this.resetUBSDistance()
          this.lat = null
          this.long = null
          if(!this.hasRequestedLocation) {
            this.hasRequestedLocation = true
            this.diagnostic.requestLocationAuthorization().then((res)=>{
              if(res!='denied'){
                console.log('Permission : '+ res)
                this.diagnostic.isLocationAuthorized().then((res)=>{
                  this.gaveLocationPermission = res;
                  if (this.gaveLocationPermission) {
                    let enablingLoader = this.loadingCtrl.create({
                      content: "Checando status do GPS..."
                    });
                  //  enablingLoader.present();
                    this.diagnostic.isLocationEnabled().then((resp) => {
                      enablingLoader.dismiss()
                      this.doneChecking = true;
                      this.hasEnabledLocation = resp;
                      if (this.hasEnabledLocation) {
                        this.getPosition();
                      }
                    });
                  }
                })
              } else { // denied location

              }
            })
          }
          this.doneChecking = true;
        }
      });
    }, err =>{
      this.gotPosition = false
      //this.resetUBSDistance()
      this.lat = null
      this.long = null
    });
  }

  getPosition() {
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
        this.gotPosition = true;

        this.alertCtrl.create({
          title: 'Resultado',
          message: 'Lat: ' + this.lat + ' ' + 'Long: ' + this.long
        }).present()

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });
  }


  alertPresent(title, message){
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }

}
